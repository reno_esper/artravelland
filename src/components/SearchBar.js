import React from 'react';
import {StyleSheet, TextInput, View} from 'react-native';

class SearchBar extends React.Component {
  constructor() {
    super();
    this.state = { searchText: '' };
  }

  render() {
    return (
      <View
        style={styles.searchView}>
        <TextInput
          style={styles.searchTextInput}
          placeholder='Search...'
          placeholderTextColor='rgba(54, 56, 46, 0.2)'
          underlineColorAndroid='transparent'
          onChangeText={(text) => this.setState({searchText: text})}/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  searchView: {
    backgroundColor: '#A7BED3',
    padding: 10
  },
  searchTextInput: {
    backgroundColor: '#BBCCDC',
    borderColor: '#ffffff',
    borderRadius: 3,
    color: '#36382E',
    height: 40,
    paddingLeft: 12,
    textAlign:'left',
  }
})

export default SearchBar;
