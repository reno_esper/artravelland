import React from 'react';
import {Text, View} from 'react-native';

class ForYou extends React.Component {
  static navigationOptions = {
    tabBarLabel: 'For You'
  };

  render() {
    return (
      <View>
        <Text>ForYou Component</Text>
      </View>
    )
  }
}

export default ForYou;
