import React from 'react';
import {Text, View} from 'react-native';

class Experience extends React.Component {
  static navigationOptions = {
    tabBarLabel: 'Experience'
  };

  render() {
    return (
      <View>
        <Text>Experience Component</Text>
      </View>
    )
  }
}

export default Experience;
