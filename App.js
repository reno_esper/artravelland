import Expo from 'expo';
import React from 'react';
import {StyleSheet, Text, TextInput, View} from 'react-native';
import {TabNavigator} from 'react-navigation';
import StatusBarBackground from './src/components/StatusBar';
import SearchBar from './src/components/SearchBar';
import ForYou from './src/screens/ForYou';
import Home from './src/screens/Home';
import Experience from './src/screens/Experience';
import Place from './src/screens/Place';

class App extends React.Component {
  render() {
    return (
      <View style={styles.mainContainer}>
        <StatusBarBackground/>
        <SearchBar/>
        <TabNavigation/>
      </View>
    );
  }
}

const TabNavigation = TabNavigator({
  ForYou: {
    screen: ForYou
  },
  Homes: {
    screen: Home
  },
  Experiences: {
    screen: Experience
  },
  Places: {
    screen: Place
  }
}, {
  tabBarOptions: {
    tabStyle: {
      width: 96,
      flex: 1
    },
    indicatorStyle: {
      backgroundColor: '#7FA2C0'
    },
    style: {
      backgroundColor: '#A7BED3'
    },
    labelStyle: {
      fontSize: 10,
      color: '#36382E'
    }
  }
});

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1
  }
});

export default App;